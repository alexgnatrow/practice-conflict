export default class Person {
    constructor(name,profession) {
        this.name = name;
        this.profession = profession || "none";
    }


    say(phrase) {
        return `${this.name} says ${phrase}. I am a ${this.profession}`;
    }


}
