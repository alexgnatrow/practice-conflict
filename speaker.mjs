import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {
        console.log(`"${super.say(phrase)}" `)
    }
}



let john = new Person('John',"millionare");
console.log(john.say('Hello!'));

let bob = new Speaker('Bob');
bob.say('Hi!');

let liza = new Speaker('Liza', 'dancer');
liza.say('Hey!');
